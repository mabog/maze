// You have a player that has a name and moves around in a maze. The maze consists of rooms with exits. 
// The rooms are in a 2D environment. Each room can have multiple exits. The player can go from one room to another.

// Each room can have certain items in it (e.g., a sword, a bomb, a banana etc.). The player has a backpack 
// that can contain a certain amount of items, depending on the item weight. So the player can pick up an item
//  in each room, but he can also drop one to pick up another.

// Please implement a command-line solution of the game that just prints out text, depending on the commands:
// - help = prints the list of commands
// - location = prints out the current player location and the list of exits
// - go to “room name” = player will move to the exit that connects the current room with the “room name” and will execute 
// the location command
// - items = prints out the list of items in the backpack
// - pick up “item name” = will pickup the item and execute the items command
// - drop “item name” = will drop the item and execute the items command
// - quit = exits the game

// You don’t have to implement the following features, but you might take them into account:
// - player has a health status and the more he walks around the more he loses
// - some rooms contain monsters and player may fight them
// - multiple players
// - some exits are impossible to pass unless the player has a key
// - some exits have to be broken given that the player has a specific item

// maze
// -rooms[{}]
// --exits

// room
// -name
// -exits[]

// player
// -bagpack[]
// -location - room 
// -goto room 
// -pick up
// -drop 

// help 
// quit 

import { stdin as input, stdout as output } from 'node:process';
// @ts-ignore
import * as readline from "readline/promises";
import { Maze } from "./maze.js";
import { Room } from "./room.js";
import { Player } from "./player.js";

const commands = ['help','location', 'maze', 'go to \'room number\'', 'bag(backpack)',
    'pick up \'item name\'','drop \'item name\'', 'quit']

const startRoom =  new Room(1, [2,3], ['sword', 'gun'])
const maze = new Maze([startRoom,
    new Room(2, [4,6], ['banana', 'beer']),
    new Room(3, [5,7]),
])
let player = new Player(startRoom)

const processLine = (line) => {
    if((line.substring(0, 4) == 'goto')){
        if(!isNaN(line.substring(5, line.length))) {
            const roomNumber = line.substring(5, line.length)
            return player.goto(maze.getRoom(roomNumber))
        }
        return commands
    }
    else {
        if(line.substring(0, 4) == 'pick'){
            const item = line.substring(8, line.length)
            return player.pickUp(item)
        }
        else {
            if(line.substring(0, 4) == 'drop'){
                const item = line.substring(5, line.length)
                return player.drop(item)
            }
        }

    }
    return commands
}

const play = (line) => {
    switch(line) {
        case 'help':
            return commands
        case 'location':
            return player.getLocation()
        case 'bag':
            return player.getbag()
        case 'maze': 
            console.log(maze)
            break
        default: {
            return processLine(line)
        }
    }
}

(async () => {
    console.log(commands)
    console.log(maze)
    const rl = readline.createInterface({ input, output });    
    while(true) {
    const answer = await rl.question(':> ');   
    if(answer == 'quit') {
        rl.close();
        break
    }
    console.log(
        play(answer)
    );
    }
})();

// const init = () => {
//     const maze = new Maze();
//     return new Player(1);
// }







