import { Room } from "./room.js";

export class Player {
    /**
     * @type {any[]}
     */
    bag
    location
    constructor(room) {
        this.bag = []
        this.location = room
    }
    goto = (room) => {
        this.location = room
        console.log('player moved to :', this.location)
    }
    getLocation = () => {
        return this.location
    }
    pickUp = (wantedItem) => {
        for (const [index, item] of Object.entries(this.location.items)) {
            if(item == wantedItem) {
                this.bag.push(item)
                break
            }
            // @ts-ignore
            if(index == this.location.items.length - 1) return `no ${wantedItem} here`
        }
        return `bag: ${this.bag}`
    }
    getbag = () => {
        return this.bag
    }
    drop(item) {
        this.bag.splice(this.bag.indexOf(item), 1)
        return `bag: ${this.bag}`
    }
}
