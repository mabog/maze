export class Maze {
    rooms 
    constructor(rooms) {
      this.rooms = rooms
    }
    getRoom(number) {
      for (const room of this.rooms) {
        if(room.number == number) {
          return room
        }
      }
    }
}
